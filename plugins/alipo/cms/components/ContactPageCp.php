<?php namespace Alipo\Cms\Components;

use Cms\Classes\ComponentBase;
use Alipo\Cms\Models\ContactPage;
use Alipo\Cms\Models\GeneralOption;
class ContactPageCp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'contactPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['generalOption'] = GeneralOption::first();
        $this->page['contactPage'] = ContactPage::first();
    }
}

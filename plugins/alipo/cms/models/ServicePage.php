<?php namespace Alipo\Cms\Models;

use Model;

/**
 * ServicePage Model
 */
class ServicePage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_cms_service_pages';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'slug',
        'description',
    ];
    public $rules = [
        'title' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'video' => 'System\Models\File',
    ];
    public $attachMany = [
        'images' => 'System\Models\File',
    ];
}

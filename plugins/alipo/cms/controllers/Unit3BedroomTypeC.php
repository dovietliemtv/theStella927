<?php namespace Alipo\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Unit3 Bedroom Type C Back-end Controller
 */
class Unit3BedroomTypeC extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Cms', 'cms', 'unit3bedroomtypec');
    }
}

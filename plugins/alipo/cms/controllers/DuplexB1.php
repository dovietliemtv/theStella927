<?php namespace Alipo\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Duplex B1 Back-end Controller
 */
class DuplexB1 extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Cms', 'cms', 'duplexb1');
    }
}

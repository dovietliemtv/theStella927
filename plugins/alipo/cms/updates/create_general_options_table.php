<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneralOptionsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_general_options')){ 
            Schema::create('alipo_cms_general_options', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('footer_text');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_general_options');
    }
}

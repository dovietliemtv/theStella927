<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicePagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_service_pages')){
            Schema::create('alipo_cms_service_pages', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->string('background');
                $table->string('description_option');
                $table->text('description');
                $table->timestamps();

            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_service_pages');
    }
}

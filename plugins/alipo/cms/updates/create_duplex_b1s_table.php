<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDuplexB1sTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_duplex_b1s')){
            Schema::create('alipo_cms_duplex_b1s', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->string('background');
                $table->string('description_option');
                $table->text('description');
                $table->timestamps();
            });    
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_duplex_b1s');
    }
}

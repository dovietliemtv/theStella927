<?php namespace Alipo\Cms;

use Backend;
use System\Classes\PluginBase;
use BackendMenu;
/**
 * Cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Cms',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Cms', 'cms', 'plugins/alipo/cms/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Cms\Components\Pages' => 'pages',
            'Alipo\Cms\Components\ContactPageCp' => 'contactpage',

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.cms.cms' => [
                'tab' => 'Cms',
                'label' => 'cms permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Content',
                'url'         => Backend::url('alipo/cms/homepage/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.cms.*'],
                'order'       => 500,
                'sideMenu' => [
                    'homepage' => [
                        'label'       => 'Home',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/homepage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'designpage' => [
                        'label'       => 'Design',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/designpage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'neighbourhoodpage' => [
                        'label'       => 'Neighbourhood',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/neighbourhoodpage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'mediapage' => [
                        'label'       => 'media',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/mediapage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'developerpage' => [
                        'label'       => 'Developer',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/developerpage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'servicepage' => [
                        'label'       => 'Service',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/servicepage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],
                    'contactpage' => [
                        'label'       => 'Contact',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/contactpage/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Pages'
                    ],

                    'unit2bedroom' => [
                        'label'       => 'Unit 2 bedroom',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/unit2bedroom/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'unit3bedroomtypea' => [
                        'label'       => 'Unit 3 bedroom type A',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/unit3bedroomtypea/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'unit3bedroomtypeb' => [
                        'label'       => 'Unit 3 bedroom type B',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/unit3bedroomtypeb/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'unit3bedroomtypec' => [
                        'label'       => 'Unit 3 bedroom type C',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/unit3bedroomtypec/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'unit4bedroom' => [
                        'label'       => 'Unit 4 bedroom',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/unit4bedroom/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'duplexa' => [
                        'label'       => 'Duplex A',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/duplexa/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'duplexb' => [
                        'label'       => 'Duplex B',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/duplexb/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'duplexb1' => [
                        'label'       => 'Duplex B1',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/duplexb1/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'penthousea' => [
                        'label'       => 'Penthouse A',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/penthousea/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'penthouseB' => [
                        'label'       => 'Penthouse B',
                        'icon'        => 'icon-area-chart',
                        'url'         => Backend::url('alipo/cms/penthouseB/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'Area'
                    ],
                    'generaloption' => [
                        'label'       => 'General Option',
                        'icon'        => 'icon-cog',
                        'url'         => Backend::url('alipo/cms/generaloption/update/1'),
                        'permissions' => ['alipo.cms.cms'],
                        'group'       => 'General Options'
                    ],
                ]
            ],
        ];
    }
}

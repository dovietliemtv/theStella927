'use strict';

var Helpers = {
    throttle: function (func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        var now = Date.now || function () {
            return new Date().getTime();
        }
        if (!options) options = {};
        var later = function () {
            previous = options.leading === false ? 0 : now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return function () {
            var _now = now();
            if (!previous && options.leading === false) previous = _now;
            var remaining = wait - (_now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        }
    },
    debounce: function (func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        var now = Date.now || function () {
            return new Date().getTime();
        }

        var later = function () {
            var last = now() - timestamp;

            if (last < wait && last >= 0) {
                timeout = setTimeout(later, wait - last);
            }
            else {
                timeout = null;
                if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) { context = args = null; }
                }
            }
        }

        return function () {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;
            if (!timeout) { timeout = setTimeout(later, wait); }
            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        }
    },
    prefixMethod: function (obj, method) {
        var pfx = ['webkit', 'moz', 'ms', 'o', ''];
        var p = 0, m, t;
        while (p < pfx.length && !obj[m]) {
            m = method;
            if (pfx[p] == '') {
                m = m.substr(0, 1).toLowerCase() + m.substr(1);
            }
            m = pfx[p] + m;
            t = typeof obj[m];
            if (t != 'undefined') {

                pfx = [pfx[p]];
                return (t == 'function' ? obj[m]() : obj[m]);
            }
            p++;
        }
    },
    isFullscreen: function () {
        return Helpers.prefixMethod(document, 'FullScreen') || Helpers.prefixMethod(document, 'IsFullScreen');
    },
    setFullscreen: function (el) {
        Helpers.prefixMethod(el, 'RequestFullScreen');
    },
    closeFullscreen: function () {
        return Helpers.prefixMethod(document, 'CancelFullScreen');
    }
};
(function ($) {

    $(document).ready(function () {
        let $body = $('body');
        let $html = $('html');
        let $window = $(window).eq(0);
        let $document = $(document).eq(0);

        //js hamburger
        $(".hamburger aside").click(function () {
            $(this).toggleClass("is-active");
            $body.toggleClass('st-show-menu');
        });
        $('.st-stoggle-arrow').on('click', function (e) {
            e.preventDefault();
            $body.toggleClass('close-content');
        });

        $('.st-nav-mobile .dropdown-toggle').on('click', function (e) {
            e.preventDefault();
            $(this).next('.dropdown-menu').slideToggle(400);
        });

        $('.st-nav-mobile .dropdown-menu li a').on('click', function (e) {
            if($(this).next('ul').length !== 0) {
                e.preventDefault();
                $(this).next('ul').slideToggle(400);
            }
        });

        $('.st-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false,
          });

          setTimeout(() => {
            $('.st-b-loading').fadeOut(400);
          }, 1500);
        detectTouchUpDown();

    });//end document ready

    function detectTouchUpDown() {
        var lastY,
            timer;
        $(document).bind('touchstart', function (e) {
            lastY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
        });
        $(document).bind('touchmove', function (e) {
            var currentY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
            if (Math.abs(currentY - lastY) < 15) { return; }
            if (currentY > lastY) {
                // console.log('down');
            } else {
                // console.log('up');
            }
        });
    }

})(jQuery);

